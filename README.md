# Gulp Boilerplate

> Tools to optimize the front-end development with Gulp.

## Requirements

-   [Node](https://nodejs.org/en/)
-   [Gulp](https://gulpjs.com/)
-   [Yarn](https://yarnpkg.com/pt-BR/) or [NPM](https://www.npmjs.com/)

## Dependencies

```yarn install``` to install all project dependencies


## Tasks

```gulp watch``` to start the server on http://localhost:3000

```gulp build --production``` to generate all production files in build folder.

## Documentation

> See the [documentation](DOCS.md) file here.

## Tasks
-   Change Lightbox to LightGallery
-   Fix volume o Plyr Audio
-   Create a documentation
