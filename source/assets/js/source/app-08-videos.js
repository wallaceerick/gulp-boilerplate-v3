var Videos = {

	init: function(){
		this.primary();
	},

	// Método para executar a chamada do video
	primary: function(){

		var element = $('.js-primary-video');

		// Verifica se existe um botão na página
		if(element.length > 0){
			// var instance = element.data('vide'); // Get the instance
			// var video = instance.getVideoObject(); // Get the video object
			// instance.destroy(); // Destroy instance
			element.vide('assets/videos/ocean', {
				volume: 1,
				muted: true,
				loop: true,
				autoplay: true,
				position: '50% 50%',
				resizing: true,
				posterType: 'jpg'
			});

		}

	}

};

Videos.init();
