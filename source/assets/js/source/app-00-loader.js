window.addEventListener('DOMContentLoaded', function() {
	'use strict'
	new QueryLoader2(document.querySelector('body'), {
		barColor: 'var(--primary)',
		backgroundColor: 'white',
		percentage: true,
		deepSearch: true,
		barHeight: 3,
		minimumTime: 1000,
		maxTime: 6000,
		fadeOutTime: 1000,
		onComplete: function() {
			console.beer('Done...')
		}
	})
})
