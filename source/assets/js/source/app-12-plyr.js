var Players = {

	init: function(){
		this.players();
		this.audio();
		this.video();
	},

	// Método para executar a chamada do plugin
	players: function(){

		var players = Plyr.setup('.js-player');

		window.players = players;

	},

	// Método para executar a chamada do plugin
	audio: function(){

		var audio = new Plyr('.js-audio');

		window.audio = audio;

	},

	video: function() {

		var video = new Plyr('.js-video', {
			title: 'Example Title',
		});

		window.video = video;
		// player.currentTime = 1;
	}

};

Players.init();