var Bootstrap = {
	init: function() {
		this.tooltip()
		this.toast()
		this.popover()
		this.dropdown()
	},
	tooltip: function() {
		var element = $('.js-tooltip')

		if (element.length > 0) {
			element.tooltip()
		}
	},
	toast: function() {
		var element = $('.js-toast')

		if (element.length > 0) {
			element.toast({
				animation: true,
				autohide: true,
				delay: 2000
			})
		}
	},
	popover: function() {
		var element = $('.js-popover')

		if (element.length > 0) {
			element.popover({
				trigger: 'focus'
			})
		}
	},
	dropdown: function() {
		var element = $('.js-dropdown-hover')

		if (element.length > 0) {
			element.hover(
				function() {
					$(this)
						.find('.dropdown-toggle')
						.addClass('active')
					$(this)
						.find('.dropdown-menu')
						.stop(true, true)
						.show()
				},
				function() {
					$(this)
						.find('.dropdown-toggle')
						.removeClass('active')
					$(this)
						.find('.dropdown-menu')
						.stop(true, true)
						.hide()
				}
			)
		}
	}
}

Bootstrap.init()
