var SweetAlert = {
	init: function() {
		this.default()
	},

	// Método para executar a chamada do plugin
	default: function() {
		var modal = $('.js-open-swal')

		if (modal.length > 0) {
			modal.click(function() {
				Swal.fire({
					icon: 'success',
					title: 'Nice!',
					html: 'Swal <strong>was opened.</strong>'
					// timer: 4000
				})
			})
		}
	}
}

SweetAlert.init()
