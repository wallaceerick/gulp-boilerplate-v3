var Masks = {
	init: function() {
		this.cep()
		this.date()
		this.time()
		this.phone()
		this.rg()
		this.cpf()
		this.cnpj()
	},
	// Method to init input masks, dependence of plugins/mask.js
	cep: function() {
		var element = $('.js-mask-cep')

		// Verify if element exists.
		if (element.length > 0) {
			element.mask('00000-000', {
				onComplete: function(cep) {
					$.ajax({
						type: 'GET',
						url: 'https://viacep.com.br/ws/' + cep + '/json/',
						dataType: 'json',
						success: function(address) {
							$('.js-set-address').val(address.logradouro)
							$('.js-set-city').val(address.localidade)
							$('.js-set-state').val(address.uf)
							$('.js-set-number').focus()
							// console.log(address)
						},
						error: function() {
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								html:
									'Ocorreu um <strong>erro ao completar</strong> seu endereço.',
								timer: 5000
							})
						}
					})
				},
				onInvalid: function(val, e, f, invalid, options) {
					var error = invalid[0]
					console.fire(val, e, f, invalid, options)
					console.fire('A letra ', error.v, ' não é permitido.')
				}
			})
		}
	},
	date: function() {
		var element = $('.js-mask-date')

		if (element.length > 0) {
			element.mask('00/00/0000', {
				placeholder: '__/__/____'
			})
		}
	},
	time: function() {
		var element = $('.js-mask-time')

		if (element.length > 0) {
			element.mask('00:00:00')
		}
	},
	phone: function() {
		var element = $('.js-mask-phone')

		if (element.length > 0) {
			element.mask('(00) 90000-0000')
		}
	},
	rg: function() {
		var element = $('.js-mask-rg')

		if (element.length > 0) {
			element.mask('00.000.000-00')
		}
	},
	cpf: function() {
		var element = $('.js-mask-cpf')

		if (element.length > 0) {
			element.mask('000.000.000-00')
		}
	},
	cnpj: function() {
		var element = $('.js-mask-cnpj')

		if (element.length > 0) {
			element.mask('00.000.000/0000-00')
		}
	}
}

Masks.init()
