var FullPage = {

	init: function(){
		this.plugin();
	},

	// Método para executar a chamada do plugin
	plugin: function(){

		var element = $('.js-fullpage');

		// Verifica se existe o elemento na página
		if(element.length > 0){

			var myFullpage = new fullpage('.js-fullpage', {
				verticalCentered: true,
				menu: '#menu',
				anchors: ['home', 'projects', 'about'],
				sectionsColor: ['#a80000', '#3e831e', '#005cef']
			});


		}

	}

};

FullPage.init();
