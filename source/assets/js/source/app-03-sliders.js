var Sliders = {

	init: function(){
		this.primary();
	},

	// Método para iniciar o slider
	// depende do arquivo plugins/slick-slider.js
	primary: function(){

		var element = $('.js-primary-slider');

		// Verifica se existe algum slider na página
		if(element.length > 0){

			element.slick({
				arrows:         true,
				dots:           true,
				infinite:       true,
				centerMode:     false,
				variableWidth:  false,
				centerPadding:  '',
				slidesToShow:   1,
				slidesPerRow:   1,
                prevArrow:      '<svg class="slick-prev"><use xlink:href="assets/images/svg.svg#left"></svg>',
                nextArrow:      '<svg class="slick-next"><use xlink:href="assets/images/svg.svg#right"></svg>',
				responsive: [
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1
					  }
					}
				]
			});

		}

	}

};

Sliders.init();
