// Modules
const gulp = require("gulp"),
	// HTML
	includes = require("gulp-file-include"),
	htmlmin = require("gulp-html-minifier"),
	nunjucks = require("gulp-nunjucks"),
	// CSS
	sass = require("gulp-sass"),
	maps = require("gulp-sourcemaps"),
	bourbon = require("bourbon").includePaths,
	// Images and SVG
	imagemin = require("gulp-imagemin"),
	// imagemin = require('imagemin'),
	// webp     = require('imagemin-webp'),
	sprite = require("gulp.spritesmith"),
	svg = require("gulp-svgstore"),
	// JS
	concat = require("gulp-concat"),
	uglify = require("gulp-uglify"),
	// Utils
	argv = require("yargs").argv,
	data = require("gulp-data"),
	header = require("gulp-header"),
	watch = require("gulp-watch"),
	notify = require("gulp-notify"),
	plumber = require("gulp-plumber"),
	sync = require("browser-sync"),
	prod = argv.production === undefined ? false : true;

const pkg = require("./package.json");
const banner = [
	"/*",
	" * Project: <%= pkg.title %> <<%= pkg.url %>>",
	" * Author: <%= pkg.author.name %> <<%= pkg.author.email %>>",
	" * Author URL: <%= pkg.author.url %>",
	" * Description: <%= pkg.description %>",
	" * Version: <%= pkg.version %>",
	"*/\n"
].join("\n");

const env = {
	source: "./source/",
	build: "./build/"
};

const src = {
	css: "assets/css/",
	js: "assets/js/",
	svg: "assets/svg/",
	images: "assets/images/",
	fonts: "assets/fonts/",
	data: "assets/data/",
	videos: "assets/videos/",
	audios: "assets/audios/"
};

const errors = {
	title: "Ooops...",
	message: "<%= error.message %>"
};

// Tasks
gulp.task(
	"sync",
	() =>
		sync({
			server: {
				baseDir: env.build
			}
		})
	// sync.init({
	// 	proxy: "http://localhost/gulp-boilerplate/build/"
	// })
);

gulp.task("reload", () => sync.reload());

gulp.task("js", () =>
	gulp
		.src([
			env.source + src.js + "libraries/jquery-3.4.1.js",
			env.source + src.js + "libraries/bootstrap.js",
			env.source + src.js + "plugins/*.js",
			env.source + src.js + "source/app-*.js"
		])
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(concat("application.js"))
		.pipe(maps.init())
		.pipe(uglify())
		.pipe(header(banner, { pkg: pkg }))
		.pipe(maps.write("./"))
		.pipe(gulp.dest(env.build + src.js))
		.pipe(sync.stream())
		.on("end", function() {
			console.log("JS Compiled and Minified");
		})
);

gulp.task("css", () =>
	gulp
		.src(env.source + src.css + "**/*.scss")
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(
			sass({
				outputStyle: prod ? "compressed" : "expanded", //nested, compact, expanded, compressed
				sourceComments: prod ? false : "map",
				sourcemaps: true,
				includePaths: [bourbon]
			}).on("error", sass.logError)
		)
		.pipe(header(banner, { pkg: pkg }))
		.pipe(gulp.dest(env.build + src.css))
		.pipe(sync.stream())
		.on("end", function() {
			console.log("CSS Compiled and Minified");
		})
);

gulp.task("html", () =>
	gulp
		.src([env.source + "*.+(html|nunjucks|json)"])
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		// .pipe(includes())
		.pipe(
			data(function() {
				return require(env.source + "data.json");
			})
		)
		.pipe(nunjucks.compile())
		.pipe(
			htmlmin({
				collapseWhitespace: true
			})
		)
		.pipe(gulp.dest(env.build))
		.on("end", function() {
			sync.reload();
			console.log("HTML Compiled and Minified");
		})
);

gulp.task("svg", () =>
	gulp
		.src(env.source + src.svg + "*.svg")
		.pipe(svg())
		.pipe(gulp.dest(env.source + src.images))
		.on("end", function() {
			console.log("All SVG files was generated");
		})
);

gulp.task("sprites", function() {
	let spriteData = gulp.src(env.source + src.images + "sprites/*.png").pipe(
		sprite({
			imgName: "../images/sprites.png",
			cssFormat: "css",
			cssName: "_sprites.scss"
		})
	);

	let imgStream = spriteData.img.pipe(gulp.dest(env.source + src.images));
	let cssStream = spriteData.css.pipe(
		gulp.dest(env.source + src.css + "components/")
	);
});

gulp.task("images", ["sprites", "svg"], () =>
	gulp
		.src([
			"!" + src.images + "sprites",
			env.source + src.images + "**/*.{jpg,png,gif,svg,ico,webp}"
		])
		.pipe(
			imagemin(
				[
					imagemin.gifsicle({ interlaced: true }),
					imagemin.jpegtran({ progressive: true }),
					imagemin.optipng({ optimizationLevel: 5 }),
					imagemin.svgo({
						plugins: [
							{ removeViewBox: true },
							{ cleanupIDs: false }
						]
					})
				],
				{
					verbose: true
				}
			)
		)
		.pipe(gulp.dest(env.build + src.images))
		.on("end", function() {
			let a = "!" + env.source + src.images + "sprites";
			console.log("All image files was optimized", a);
		})
);

gulp.task("fonts", () =>
	gulp
		.src([env.source + src.fonts + "*.{eot,svg,ttf,woff,woff2}"], {
			base: ""
		})
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(gulp.dest(env.build + src.fonts))
		.on("end", function() {
			console.log("Generate Font Files");
		})
);

gulp.task("videos", () =>
	gulp
		.src([env.source + src.videos + "*.{mp4,webm,ogv,jpg,vtt}"], {
			base: ""
		})
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(gulp.dest(env.build + src.videos))
		.on("end", function() {
			console.log("Generate Video Files");
		})
);

gulp.task("audios", () =>
	gulp
		.src([env.source + src.audios + "*.{mp3,ogg,wav,wma}"], { base: "" })
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(gulp.dest(env.build + src.audios))
		.on("end", function() {
			console.log("Generate Audio Files");
		})
);

gulp.task("data", () =>
	gulp
		.src([env.source + src.data + "*.{json,js,txt,md}"], { base: "" })
		.pipe(
			plumber({
				errorHandler: notify.onError(errors)
			})
		)
		.pipe(gulp.dest(env.build + src.data))
		.on("end", function() {
			console.log("Generate Data Files");
		})
);

gulp.task("assets", [
	"sprites",
	"svg",
	"fonts",
	"images",
	"videos",
	"audios",
	"data"
]);

gulp.task("watch", ["html", "js", "css", "assets", "sync"], function() {
	gulp.watch(env.source + src.css + "**/*.scss", ["css"]);
	gulp.watch(
		[env.source + src.js + "**/*.js", "!./assets/js/application.js"],
		["js"]
	);
	gulp.watch(env.source + "**/*.+(html|nunjucks|json)", ["html"]);
});

gulp.task("default", ["watch"]);

gulp.task("build", ["html", "js", "css", "assets"]);
